import HomePage from './pages/home.vue';
import SessionPage from './pages/session.vue';
import InfoPage from './pages/Info';
import SessionTablePage from './pages/session/SessionTable.vue';
import UserTablePage from './pages/session/UserTable.vue';
import EmailPage from './pages/sendEmail.vue';

export default [
  {
    path: '/',
    component: HomePage,
  },
  {
    path: '/session/:sid/:uid',
    component: SessionPage,
  },
];
