import firebase from 'firebase';
const config = {
  apiKey: "AIzaSyC1Ljt6qZZiQjJYUJRiOHyEWXRuHQkekVk",
  authDomain: "makeup-scheduler-app.firebaseapp.com",
  databaseURL: "https://makeup-scheduler-app.firebaseio.com",
  projectId: "makeup-scheduler-app",
  storageBucket: "makeup-scheduler-app.appspot.com",
  messagingSenderId: "470900145053"
};
firebase.initializeApp(config);

export default firebase
export const db = firebase.database();
export const auth = firebase.auth();
export const func = firebase.functions();
